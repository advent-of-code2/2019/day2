cmake_minimum_required(VERSION 2.8)

project(Part2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -Wall -Wextra")

add_executable(Part2 main.cpp)
