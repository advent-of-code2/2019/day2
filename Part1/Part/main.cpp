#include <fstream>
#include <iostream>
#include <string>
#include <vector>

typedef std::vector<int> IntVector;

void getOpCodes(IntVector *opcodes) {
  std::ifstream file;
  file.open("input.txt");
  int mass;
  if (file.is_open()) {
    while (file >> mass) {
      opcodes->push_back(mass);
    }
    file.close();
  } else {
    std::cerr << "ERROR: Failed to open input file.";
    exit(EXIT_FAILURE);
  }
}

int main() {
  IntVector opcodes;
  getOpCodes(&opcodes);
  opcodes[1] = 12;
  opcodes[2] = 2;

  int position = 0;
  int opcode = opcodes[position];
  while (opcode != 99) {
    if (opcode == 1) {
      opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] + opcodes[opcodes[position + 2]];
    } else if (opcode == 2) {
      opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] * opcodes[opcodes[position + 2]];
    } else {
      std::cerr << "ERROR: Unknown opcode detected." << std::endl;
      exit(EXIT_FAILURE);
    }
    position += 4;
    opcode = opcodes[position];
  }

  std::cout << "Opcodes[0] = " << opcodes[0] << std::endl;
}
